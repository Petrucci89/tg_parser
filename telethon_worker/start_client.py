from decouple import config
from telethon import TelegramClient

TG_API_ID = int(config('TG_API_ID'))
TG_API_HASH = config('TG_API_HASH')
TG_PHONE_NUMBER = config('TG_PHONE_NUMBER')

client = TelegramClient(f"{TG_PHONE_NUMBER}.session",
                        api_id=TG_API_ID,
                        api_hash=TG_API_HASH)

client.start(phone=TG_PHONE_NUMBER)
# Signed in successfully as My Test 2

# docker-compose run telethon_worker python start_client.py
