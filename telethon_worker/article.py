import datetime
import json
import re

from dataclasses import dataclass

from typing import Optional


REGEX = r'^(.*?)["](?P<report_name>(.*?))["]((\r|\n)\s*(\r|\n))(?P<title>(.*?))((\r|\n)\s*(\r|\n))(([И]|[и])сточник:)[\s]?(?P<source>(.*?))((\r|\n)*(\r|\n))(([А]|[а])втор:[\s]?)(?P<author>(.*?))((\r|\n)*(\r|\n))(([Д]|[д])ата:[\s]?)(?P<date>(.*?))((\r|\n)\s*(\r|\n))(?P<content>(([.]|[\s]|[\r]|[\n]|[\S])+_?))([\n|\r])*(?P<url>(http[s]?:\/\/\S+))$'
HOST_REGEX = r'^(http[s]?)://(www.)?(\w+[.]\w+[.]?\w+)'
DATE_REGEX = r'^(?P<day>\d+)\s(?P<month>\w+)\s(?P<year>\d+)\s?(?P<time>\d+:\d+(:\d+)?)?$'


PATTERN = re.compile(REGEX, re.IGNORECASE)
HOST_PATTERN = re.compile(HOST_REGEX, re.IGNORECASE)
DATE_PATTERN= re.compile(DATE_REGEX, re.IGNORECASE)


class PubTime:
    monthes = {
        "января": 1,
        "февраля": 2,
        "марта": 3,
        "апреля": 4,
        "мая": 5,
        "июня": 6,
        "июля": 7,
        "августа": 8,
        "сентября": 9,
        "октября": 10,
        "ноября": 11,
        "декабря": 12
    }

    def __new__(cls, dt_str: str) -> int:
        data = DATE_PATTERN.match(dt_str)
        day = int(data.group('day'))
        month = cls.monthes[data.group('month').lower()]
        year = int(data.group('year'))
        d = datetime.date(year, month, day)
        time_str = data.group('time') or '00:00:00'
        try:
            t = datetime.datetime.strptime(time_str, '%H:%M:%S').time()
        except ValueError:
            t = datetime.datetime.strptime(time_str, '%H:%M').time()
        return int(datetime.datetime.combine(d, t).timestamp())


@dataclass
class Article:
    url: str
    channel_id: int
    host_url: str
    pub_time: int
    fetch_time: int
    title: str
    text: str

    host: Optional[str] = None
    text_time: Optional[int] = None
    author: Optional[str] = None

    source_type: str = "telegram"
    report_name: Optional[str] = None

    def to_json(self):
        return json.dumps(self, ensure_ascii=False, default=lambda x: x.__dict__)

    @classmethod
    def build_from_text(cls, input_text: str, channel_id: int, pub_time: int, fetch_time: int):
        data = re.search(PATTERN, input_text)

        try:
            url = data.group('url')
            host_url = None
            _host = HOST_PATTERN.match(url)
            if _host:
                host_url = _host.group(3)
            host = data.group('source')
            text_time = PubTime(data.group('date'))
            title = data.group('title')
            text = data.group('content')
            author = data.group('author') or None
            report_name = data.group('report_name')
        except:
            raise Exception("Неправильный формат сообщения")

        return cls(url, channel_id, host_url, pub_time, fetch_time, title, text, host, text_time, author, report_name=report_name)




