import aio_pika
import asyncio
import logging
import os
import sys

from article import Article
from decouple import config
import datetime
from telethon import TelegramClient, events
from telethon.tl import functions, types

TG_API_ID = int(config('TG_API_ID'))
TG_API_HASH = config('TG_API_HASH')

TG_PHONE_NUMBER = config('TG_PHONE_NUMBER')

AMQP_HOST = config('AMQP_HOST')
AMQP_PORT = int(config('AMQP_PORT'))
AMQP_USER = config('RABBITMQ_DEFAULT_USER')
AMQP_PASS = config('RABBITMQ_DEFAULT_PASS')

CHANNEL_IDS = [int(channel_id) for channel_id in config('CHANNEL_IDS').split(',')]

LOG_LEVEL = config('LOG_LEVEL')
LOG_FILE_NAME = config('LOG_FILE_NAME')


logging.basicConfig(filename=f"./{LOG_FILE_NAME}",
                    level=LOG_LEVEL,
                    format="[%(asctime)s] %(levelname)s %(message)s",
                    datefmt="%m.%d.%Y %H:%M:%S")

client = TelegramClient(f"{TG_PHONE_NUMBER}.session",
                        api_id=TG_API_ID,
                        api_hash=TG_API_HASH)


CONNECTION = None


async def messageHandler(update):

    try:

        if not hasattr(update.message, 'message'):
            return

        if not update.message.message:
            return

        if update.message.from_id not in CHANNEL_IDS:
            return

        now = int(datetime.datetime.utcnow().timestamp())
        article = Article.build_from_text(update.message.message, update.from_id, now, now)
        article_json_string = article.to_json()

        logging.info(f"{update.from_id} {article.title} get new message")

        channel = await CONNECTION.channel()

        await channel.default_exchange.publish(
            aio_pika.Message(
                body=article_json_string.encode()
            ),
            routing_key="documents"
        )

        logging.info(f"{article.title} send to queue")

    except Exception as e:

        exc_type, exc_obj, exc_tb = sys.exc_info()
        f_name = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error_str = f"messageHandler {str(e)} {exc_type} {f_name} {exc_tb.tb_lineno}"
        logging.error(error_str)


async def start_client():

    global CONNECTION

    try:

        CONNECTION = await aio_pika.connect_robust(
            host=AMQP_HOST,
            port=AMQP_PORT,
            login=AMQP_USER,
            password=AMQP_PASS
        )

        await client.connect()

        client.add_event_handler(messageHandler, event=events.NewMessage)

        while True:
            await client(functions.updates.GetStateRequest())
            await asyncio.sleep(100)


    except Exception as e:

        exc_type, exc_obj, exc_tb = sys.exc_info()
        f_name = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error_str = f"start_client {str(e)} {exc_type} {f_name} {exc_tb.tb_lineno}"
        logging.error(error_str)


try:

    loop = asyncio.get_event_loop()

    loop.run_until_complete(start_client())

except KeyboardInterrupt:

    pass

finally:

    loop.stop()
    loop.close()

